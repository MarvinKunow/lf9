import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVDateiLesenBeispiel {

	public static void main(String[] args) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("artikelliste.csv"));
			String zeile = "";

			while(zeile != null) {
				zeile = reader.readLine();
				System.out.println(zeile);
			}

			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
